class TestBugH2 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {

    public int f() {
	int result;
	int count;
	result = 0;
	count = 1;
	do {
	    result = result + count;
	    count = count + 1;
	} while (10 < count); // Runtime error: 10 will never be less than count as count will be 2 in the first iteration.
    // Should really be 10 > count to get the expected output.
	return result;
    }

}

// Will compile fine but output isn't as expected due to a mistake in logic.
// Note: This test and the last test were very confusing to me and I spent hours researching and testing
// a variety of different ways of correcting the logic in the compiler but came to the conclusion that
// it was out of scope for me. I have left the code as is and noted the error in the comments.
// I don't believe there is any error within the generated code or the compiler itself. But rather the actual code given to the compiler.
